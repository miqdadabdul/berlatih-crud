<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class castController extends Controller
{
    //CRUD Create
    public function create(){
        return view('cast.create');
    }

    public function store(Request $request){
        $request->validate([
            'nama' => 'required', //artinya harus diisi
            'umur' => 'required',
            'bio' => 'required',
        ]);

        DB::table('cast')->insert([
            'nama' => $request['nama'], //"nama =>" itu sesuai di database
            'umur' => $request['umur'],
            'bio' => $request['bio']
        ]);    

        return redirect('/cast');
    }

    //CRUD Read data yg di buat
    public function index(){
        $cast = DB::table('cast')->get();

        return view('cast.index', compact('cast'));
    }

    //CRUD pas klik tombol detail
    public function show($id){
        $cast = DB::table('cast')->where('id', $id)->first();
        return view('cast.show', compact('cast'));
    }

    //CRUD Edit , edit doank, klo biar datanya berubah pake crud update juga
    public function edit($id){
        $cast = DB::table('cast')->where('id', $id)->first();
        return view('cast.edit', compact('cast'));
    }

    //CRUD Update
    public function update($id, Request $request){
        $request->validate([
            'nama' => 'required', //artinya harus diisi
            'umur' => 'required',
            'bio' => 'required',
        ]);

        $query = DB::table('cast')
              ->where('id', $id)
              ->update([
                  'nama' => $request['nama'],
                  'umur' => $request['umur'],
                  'bio' => $request['bio']
                ]);

        return redirect('/cast');
    }

    //CRUD Delete data
    public function destroy($id){
        DB::table('cast')->where('id', $id)->delete();
        return redirect('/cast');
    }
}
